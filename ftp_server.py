"""
ftp 文件服务器
分为服务端和客户端，要求可以有多个客户端
同时操作。
客户端可以查看服务器文件库中有什么文件。
客户端可以从文件库中下载文件到本地。
客户端可以上传一个本地文件到文件库。
使用print在客户端打印命令输入提示，
引导操作

模块划分

通信协议的设计

具体模块逻辑

LIST  STOR  RETR  EXIT
lalallalala
"""
from threading import Thread
from socket import *
from time import sleep
import sys
import os

FTP = "/home/tarena/FTP/"


class Handle:
    def __init__(self, connfd):
        self.connfd = connfd

    def handle(self):
        while True:
            request = self.connfd.recv(1024).decode()
            tmp = request.split(" ")
            if not request or tmp[0] == "EXIT":
                break
            elif tmp[0] == "LIST":
                self.do_list()
            elif tmp[0] == "STOR":
                self.upload(tmp[1])
            elif tmp[0] == "RETR":
                self.download(tmp[1])
        self.connfd.close()

    def do_list(self):
        filelist = os.listdir(FTP)
        if filelist:
            self.connfd.send(b"OK")
            sleep(0.1)
            # 增加消息边界 \n 防止粘包
            files = "\n".join(filelist)
            self.connfd.send(files.encode())
        else:
            self.connfd.send(b"FAIL")

    def download(self, filename):
        try:
            file = open(FTP + filename, 'rb')
        except:
            self.connfd.send(b"FAIL")
        else:
            self.connfd.send(b"OK")
            sleep(0.1)
            while True:
                data = file.read(1024)
                if not data:
                    break
                self.connfd.send(data)
            sleep(0.1)
            self.connfd.send(b"##")
            file.close()

    def upload(self, filename):
        if os.path.exists(FTP + filename):
            self.connfd.send(b"FAIL")
        else:
            self.connfd.send(b"OK")
            file = open(FTP + filename, 'wb')
            while True:
                data = self.connfd.recv(1024)
                if data == b"##":
                    break
                file.write(data)
            file.close()


class ThreadServer(Thread):
    def __init__(self, connfd):
        self.connfd = connfd
        self.handle = Handle(connfd)
        super().__init__()

    def run(self):
        self.handle.handle()


class FTPServer:
    def __init__(self, host="", port=0):
        self.host = host
        self.port = port
        self.addr = (host, port)
        self.sock = self._create_socket()

    def _create_socket(self):
        sock = socket()
        sock.bind(self.addr)
        return sock

    def serve_forevr(self):
        self.sock.listen(5)
        print("Listen the port %d" % self.port)
        while True:
            try:
                connfd, addr = self.sock.accept()
                print("Connect from", addr)
            except KeyboardInterrupt:
                self.sock.close()
                sys.exit("服务结束")

            t = ThreadServer(connfd)
            t.start()


if __name__ == '__main__':
    ftp = FTPServer(host="0.0.0.0", port=4631)
    ftp.serve_forevr()
