"""
http  请求和响应
"""
from socket import *

sock = socket()
sock.bind(("0.0.0.0", 4631))
sock.listen(5)

connfd, addr = sock.accept()
print("Connect from", addr)

request = connfd.recv(1024)
print(request.decode())

#组织响应格式
response="""HTTP/1.1 200 OK
Content-Type:text/html

Hello World
"""
connfd.send(response.encode())


connfd.close()
sock.close()
