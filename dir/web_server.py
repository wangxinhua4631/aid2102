"""
web 服务程序实现

"""

from socket import *
from select import *
import os


class Handle:
    def __init__(self, html):
        self.html = html

    def _response(self, status, filename):
        response = "HTTP/1.1 %s OK\r\n" % status
        response += "Content-Type:text/html\r\n"
        response += "\r\n"
        with open(filename, "rb") as file:
            response = response.encode() + file.read()
        return response

    def _send_response(self, connfd, info):
        if info == "/":
            filename = self.html + "/index.html"
        else:
            filename = self.html + info

        if os.path.exists(filename):
            data = self._response("200 OK", filename)
        else:
            data = self._response("404 NOT Found", self.html + "/404.html")
        connfd.send(data)

    def handle(self, connfd):
        request = connfd.recv(1024).decode()
        if not request:
            return
        info = request.split(" ")[1]
        self._send_response(connfd, info)


class WebServer:
    def __init__(self, host=" ", port=0, html=None):
        self.host = host
        self.port = port
        self.html = html
        self.address = (host, port)
        self.rlist = []
        self.handle = Handle(html)
        self.sock = self._create_sock()

    def _create_sock(self):
        sock = socket()
        sock.bind(self.address)
        sock.setblocking(False)
        return sock

    def _connect(self):
        connfd, addr = self.sock.accept()
        print("Connent from", addr)
        connfd.setblocking(False)
        self.rlist.append(connfd)

    def start(self):
        self.sock.listen(5)
        print("Listen the port %d" % self.port)
        self.rlist.append(self.sock)
        while True:
            rs, ws, xs = select(self.rlist, [], [])
            for r in rs:
                if r is self.sock:
                    self._connect()
                else:
                    try:
                        self.handle.handle(r)
                    except:
                        pass
                    self.rlist.remove(r)
                    r.close()


if __name__ == '__main__':
    httpd = WebServer(host="0.0.0.0", port=4631, html="./static")
    httpd.start()
