"""
ftp 客户端
"""
from socket import *
from time import sleep
import sys

ADDR = ("176.233.33.8", 4631)


class FTPClient:
    def __init__(self):
        self.sock = self._connect()

    def _connect(self):
        sock = socket()
        sock.connect(ADDR)
        return sock

    def do_list(self):
        self.sock.send(b"LIST")
        response = self.sock.recv(128)
        if response == b"OK":
            files = self.sock.recv(1024 * 1024)
            print(files.decode())
        else:
            print("文件库为空")

    def download(self, filename):
        request = "RETR " + filename
        self.sock.send(request.encode())
        response = self.sock.recv(128)
        if response == b"OK":
            file = open(filename, "wb")
            while True:
                data = self.sock.recv(1024)
                if data == b"##":
                    break
                file.write(data)
            file.close()
        else:
            print("文件不存在,请重新输入文件名称")

    def upload(self, filename):
        try:
            file = open(filename, 'rb')
        except:
            print("文件不存在,请重新输入")
            return
        filename = filename.split("/")[-1]
        request = "STOR " + filename
        self.sock.send(request.encode())
        response = self.sock.recv(128)
        if response == b"OK":
            while True:
                data = file.read(1024)
                if not data:
                    break
                self.sock.send(data)
            sleep(0.1)
            self.sock.send(b"##")
            file.close()
        else:
            print("该文件已存在")

    def do_exit(self):
        self.sock.send(b"EXIT")
        self.sock.close()
        sys.exit("谢谢使用")


class FTPView:
    def __init__(self):
        self.handle = FTPClient()

    def __display_menu(self):
        print("""
                ======== FTP 服务 =======
                |    1.查 看 文 件       |
                |    2.下 载 文 件       |
                |    3.上 传 文 件       |
                |    4.退 出 系 统       |
                =========================
                """)

    def __select_menu(self):
        item = input("请输入选项:")
        if item == "1":
            self.handle.do_list()
        elif item == "2":
            filename = input("要下载的文件:")
            self.handle.download(filename)
        elif item == "3":
            filename = input("要上传的文件:")
            self.handle.upload(filename)
        elif item == "4":
            self.handle.do_exit()
        else:
            print("请输入正确选项")

    def main(self):
        while True:
            self.__display_menu()
            self.__select_menu()


if __name__ == '__main__':
    ftp = FTPView()
    ftp.main()
